package main

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func (app *Application) routes() http.Handler {
	router := httprouter.New()
	router.NotFound = http.HandlerFunc(app.notFoundResponse)

	router.MethodNotAllowed = http.HandlerFunc(app.methodNotAllowedResponse)

	router.HandlerFunc(http.MethodGet, "/v1/healthCheck", app.healthcheckHandler)
	//router.HandlerFunc(http.MethodPost, "/v1/createModule", app.requirePermission("module:write", app.CreateModuleHandler))
	router.HandlerFunc(http.MethodPost, "/v1/createModule", app.CreateModuleHandler)

	router.HandlerFunc(http.MethodGet, "/v1/getModule/:id", app.GetModuleHandler)

	//router.HandlerFunc(http.MethodGet, "/v1/getModule/:id", app.requirePermission("movies:read", app.GetModuleHandler))
	router.HandlerFunc(http.MethodGet, "/v1/getModule", app.listModuleHandler)
	router.HandlerFunc(http.MethodPut, "/v1/updateModule/:id", app.requirePermission("movies:write", app.editModuleHandler))
	router.HandlerFunc(http.MethodDelete, "/v1/deleteModule/:id", app.requirePermission("movies:write", app.deleteModuleHandler))

	router.HandlerFunc(http.MethodDelete, "/v1/DeleteUsers/:id", app.deleteUserHandler)
	router.HandlerFunc(http.MethodGet, "/v1/getTeachers", app.requireActivatedUser(app.listTeacherHandler))
	router.HandlerFunc(http.MethodPost, "/v1/users", app.registerUserHandler)
	router.HandlerFunc(http.MethodPut, "/v1/users/activated", app.activateUserHandler)
	router.HandlerFunc(http.MethodPost, "/v1/tokens/authentication", app.createAuthenticationTokenHandler)
	return app.recoverPanic(app.rateLimit(app.authenticate(router)))

}
