package data

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestInsert(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := DBModel{DB: db}

	rows := sqlmock.NewRows([]string{"id", "created_at", "updated_at", "version"}).
		AddRow(1, time.Now(), time.Now(), 1)

	mock.ExpectQuery("^INSERT INTO module_info").
		WithArgs("Test Module", 60, "Final").
		WillReturnRows(rows)

	module := &ModuleInfo{
		ModuleName:     "Test Module",
		ModuleDuration: 60,
		ExamType:       "Final",
	}

	err = m.Insert(module)
	if err != nil {
		t.Errorf("error was not expected while inserting record: %s", err)
	}

	if module.ID != 1 {
		t.Errorf("expected ID to be '1', got '%d'", module.ID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGet(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := DBModel{DB: db}

	rows := sqlmock.NewRows([]string{"id", "created_at", "updated_at", "module_name", "module_duration", "exam_type", "version"}).
		AddRow(1, time.Now(), time.Now(), "Test Module", 60, "Final", 1)

	mock.ExpectQuery("^SELECT id, created_at, updated_at, module_name, module_duration, exam_type, version FROM module_info WHERE id =").
		WithArgs(1).
		WillReturnRows(rows)

	module, err := m.Get(1)
	if err != nil {
		t.Errorf("error was not expected while getting record: %s", err)
	}

	if module.ID != 1 {
		t.Errorf("expected ID to be '1', got '%d'", module.ID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
func TestUpdate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := DBModel{DB: db}

	rows := sqlmock.NewRows([]string{"version"}).
		AddRow(2)

	mock.ExpectQuery("^UPDATE module_info SET").
		WithArgs("Updated Module", 120, "Midterm", 1, 1).
		WillReturnRows(rows)

	module := &ModuleInfo{
		ID:             1,
		ModuleName:     "Updated Module",
		ModuleDuration: 120,
		ExamType:       "Midterm",
		Version:        1,
	}

	err = m.Update(module)
	if err != nil {
		t.Errorf("error was not expected while updating record: %s", err)
	}

	if module.Version != 2 {
		t.Errorf("expected Version to be '2', got '%d'", module.Version)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestDelete(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := DBModel{DB: db}

	mock.ExpectExec("^DELETE FROM module_info WHERE id =").
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = m.Delete(1)
	if err != nil {
		t.Errorf("error was not expected while deleting record: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
