package data

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestInsertUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := UserModel{DB: db}

	rows := sqlmock.NewRows([]string{"id", "created_at", "updated_at", "version"}).
		AddRow(1, time.Now(), time.Now(), 1)

	mock.ExpectQuery("^INSERT INTO user_info").
		WithArgs("Test", "User", "testuser@example.com", []byte("hashedpassword"), "user", false).
		WillReturnRows(rows)

	user := &User{
		FName:     "Test",
		SName:     "User",
		Email:     "testuser@example.com",
		Password:  Password{Hash: []byte("hashedpassword")},
		UserRole:  "user",
		Activated: false,
	}

	err = m.Insert(user)
	if err != nil {
		t.Errorf("error was not expected while inserting record: %s", err)
	}

	if user.ID != 1 {
		t.Errorf("expected ID to be '1', got '%d'", user.ID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestDeleteUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := UserModel{DB: db}

	mock.ExpectExec("^DELETE FROM user_info WHERE id =").
		WithArgs(1).
		WillReturnResult(sqlmock.NewResult(1, 1))

	err = m.DeleteUser(1)
	if err != nil {
		t.Errorf("error was not expected while deleting record: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetByEmail(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := UserModel{DB: db}

	rows := sqlmock.NewRows([]string{"id", "created_at", "updated_at", "fname", "sname", "email", "password_hash", "user_role", "activated", "version"}).
		AddRow(1, time.Now(), time.Now(), "Test", "User", "testuser@example.com", []byte("hashedpassword"), "user", true, 1)

	mock.ExpectQuery("^SELECT id, created_at, updated_at, fname, sname, email, password_hash, user_role, activated, version FROM user_info WHERE email =").
		WithArgs("testuser@example.com").
		WillReturnRows(rows)

	user, err := m.GetByEmail("testuser@example.com")
	if err != nil {
		t.Errorf("error was not expected while getting record: %s", err)
	}

	if user.ID != 1 {
		t.Errorf("expected ID to be '1', got '%d'", user.ID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetUser(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	m := UserModel{DB: db}

	rows := sqlmock.NewRows([]string{"id", "created_at", "fname", "sname", "email", "user_role", "activated", "version"}).
		AddRow(1, time.Now(), "Test", "User", "testuser@example.com", "user", true, 1)

	mock.ExpectQuery("^SELECT id, created_at, fname, sname, email, user_role, activated, version FROM user_info WHERE id =").
		WithArgs(1).
		WillReturnRows(rows)

	user, err := m.GetUser(1)
	if err != nil {
		t.Errorf("error was not expected while getting record: %s", err)
	}

	if user.ID != 1 {
		t.Errorf("expected ID to be '1', got '%d'", user.ID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
